angular.module('App').controller('homeController', function(jsonrpc, uiGridConstants) {

	var self = this;
	self.error = {};

	self.gridOptions = {
		enableFiltering: true,
		columnDefs: [
			{
				field: 'id',
				width: 100,
			},
			{
				field: 'url',
			},
			{
				field: 'when_created',
				enableFiltering: false,
				width: 160,
			},
			{
				field: 'when_sent',
				enableFiltering: false,
				width: 160,
			},
			{
				name: 'Status',
				field: 'status',
				width: 160,
				filter: {
					type: uiGridConstants.filter.SELECT,
					selectOptions: [
						{value: 'pending', label: 'pending'},
						{value: 'success', label: 'success'},
						{value: 'failed', label: 'failed'},
					],
				},
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					return 'bg-' + grid.getCellValue(row, col);
				},
			},
		],
		data: [],
	};

	jsonrpc.request('message.getList', ['sdfsdf'])
		.then(function(result) {
			self.gridOptions.data = result;
		})
		.catch(function(error) {
			self.error = error;
		});

});