angular.module('App').controller('baseController', function($location) {

	var self = this;
	self.appName = 'OpenProvider HTTP Sender';

	self.isActive = function(route) {
		return $location.path() == route;
	};

});