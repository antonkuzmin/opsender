angular.module('App').controller('createController', function(jsonrpc) {

	var self = this;
	self.form = {};

	self.submit = function() {
		if (self.form.url && self.form.body)
			jsonrpc.request('message.create', self.form)
				.then(function(result) {
					window.location = '#/';
				})
				.catch(function(error) {
					alert('Не удалось создать запрос: ' + error);
				});
	};

});