App = angular.module('App', ['ngRoute', 'ui.grid', 'angular-jsonrpc-client'])
	.config(function($routeProvider, $locationProvider, jsonrpcConfigProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/home.html',
			})
			.when('/create', {
				templateUrl: 'views/create.html',
			});

		jsonrpcConfigProvider.set({
			url: '/api/jsonrpc',
		});
	});