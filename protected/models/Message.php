<?php

class Message extends CActiveRecord
{

	const STATUS_TO_SEND = 1;
	const STATUS_SUCCESS = 2;
	const STATUS_ERROR = 3;

	public $statusLabels = [
		self::STATUS_TO_SEND => 'pending',
		self::STATUS_SUCCESS => 'success',
		self::STATUS_ERROR => 'failed',
	];

	public function tableName()
	{
		return 'message';
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if ($this->isNewRecord)
			$this->status = self::STATUS_TO_SEND;
		return parent::beforeSave();
	}

	public function send()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->url);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $this->body);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

		$this->response = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$this->status = $code == 200 ? self::STATUS_SUCCESS : self::STATUS_ERROR;
		$this->when_sent = new CDbExpression('now()');
		curl_close($curl);
		$this->save();
		return $this->status;
	}

	public function toJson()
	{
		return [
			'id' => $this->id,
			'url' => $this->url,
			'when_created' => $this->when_created,
			'when_sent' => $this->when_sent,
			'status' => isset($this->statusLabels[$this->status]) ? $this->statusLabels[$this->status] : $this->status,
		];
	}

}