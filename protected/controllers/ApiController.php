<?php

class ApiController extends CController
{

	const MAX_REQUESTS = 100;

	public function actionIndex()
	{
		$_GET['smd'] = 1;
		$this->actionJsonrpc();
	}

	public function actionJsonrpc()
	{
		$server = new BaseJsonRpcServer();
		foreach (glob('protected/api/*Api.php') as $file) {
			$className = pathinfo($file, PATHINFO_FILENAME);
			$instance = new $className;
			$server->RegisterInstance($instance, strtolower(str_replace('Api', '', $className)));
		}
		$server->execute();
	}

	public function actionCron()
	{
		$res = array_fill(2, 2, 0);
		$criteria = (new CDbCriteria(['limit' => self::MAX_REQUESTS, 'order' => 'id asc']))
			->compare('status', Message::STATUS_TO_SEND);
		foreach (Message::model()->findAll($criteria) as $message) {
			$message->send();
			$res[$message->status]++;
		}
		echo 'Success: '.$res[2].'; Error: '.$res[3];
	}

}