
SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+07:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(4000) NOT NULL,
  `body` mediumtext NOT NULL,
  `when_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `when_sent` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `response` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
