<?php
return CMap::mergeArray(require(dirname(__FILE__).'/app.php'), [
	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=opsender',
			'username' => 'root',
			'password' => '',
			'initSQLs' => ['SET NAMES utf8;'],
			'enableProfiling' => true,
			'enableParamLogging' => true,
		],
	]
]);