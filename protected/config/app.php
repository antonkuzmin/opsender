<?php
return [
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'OpenProvider HTTP Sender',

	'sourceLanguage' => 'ru',
	'language' => 'ru',
	'timeZone' => 'Asia/Krasnoyarsk',

	'preload' => ['log'],

	'import' => [
		'application.api.*',
		'application.models.*',
		'application.components.*',
		'application.vendor.easy-jsonrpc.*',
	],

	'defaultController' => 'api',

	'components' => [
		'db' => [
			'initSQLs' => ["set time_zone='+07:00';"],
		],
		'session' => [
			'autoStart' => true,
		],
		'format' => [
			'class' => 'CFormatter',
			'dateFormat' => 'd.m.Y',
			'datetimeFormat' => 'd.m.Y H:i',
		],
		'urlManager' => [
			'urlFormat' => 'path',
			'rules' => [
				'<action>' => 'api/<action>',
			],
		],
	],
];