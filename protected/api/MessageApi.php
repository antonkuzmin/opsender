<?php


class MessageApi
{

	public function getList()
	{
		return array_map(function ($message) {
			return $message->toJson();
		}, Message::model()->findAll(['order' => 'id desc']));
	}

	public function create($url, $body)
	{
		$message = new Message;
		$message->url = $url;
		$message->body = $body;
		return $message->save();
	}

}