<?php
error_reporting(E_ALL);

call_user_func(function() {
	$host = $_SERVER['SERVER_NAME'];
	$envs = [
		'localhost' => 'dev',
		'opsender.antonkuzmin.ru' => 'prod',
	];
	if (isset($envs[$host]))
		define('APP_ENV', $envs[$host]);
	else
		die("<h1>Incorrect Host: $host</h1>");
	if (APP_ENV != 'prod') {
		define('YII_DEBUG', true);
		ini_set('display_errors', 1);
	}
});

require_once('protected/vendor/yiisoft/yii.php');
Yii::createWebApplication('protected/config/'.APP_ENV.'.php')->run();
